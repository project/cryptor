<?php

namespace Drupal\sessionless;

use Drupal\Core\Logger\LoggerChannelInterface;
use Jose\Component\Encryption\Compression\Deflate;
use Jose\Easy\Build;
use Jose\Easy\Load;

/**
 * Sessionless encryption service.
 *
 * @internal
 *
 * https://connect2id.com/products/nimbus-jose-jwt/examples/signed-and-encrypted-jwt
 */
final class CryptoService {

  protected CryptoKeyStorage $keyStorage;

  protected LoggerChannelInterface $logger;

  public function __construct(CryptoKeyStorage $keyStorage, LoggerChannelInterface $logger) {
    $this->keyStorage = $keyStorage;
    $this->logger = $logger;
  }


  public function dump($data, bool $encrypt = TRUE): string {
    $packed = $this->packData($data);
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $jwtString = $encrypt ? $this->signAndEncrypt($packed) : $this->sign($packed);
    return $jwtString;
  }

  public function parse(string $token, bool $encrypt = TRUE) {
    try {
      $packed = $encrypt ? $this->parseSignedAndEncrypted($token) : $this->parseSigned($token);
      $data = $this->unpackData($packed);
      // @todo Catch only relevant exceptions.
    } catch (\Exception $e) {
      $this->logger->notice('Unable to parse: @jwt', ['@jwt' => $token]);
      return NULL;
    }
    return $data;
  }

  protected function sign($data, array $headers = [], array $claims = []): string {
    $jws = Build::jws()
      ->alg('ES256')
      ->payload(['d' => $data] + $claims);
    foreach ($headers as $headerKey => $headerValue) {
      $jws->header($headerKey, $headerValue);
    }
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $signed = $jws->sign($this->keyStorage->getSignatureKey());
    return $signed;
  }

  protected function parseSigned(string $token) {
    /** @var \Jose\Easy\JWT $jwt */
    $jwt = Load::jws($token)
      ->key($this->keyStorage->getSignatureKey())
      ->run();
    return $jwt->claims->get('d');
  }

  protected function encrypt($data, array $headers = [], array $claims = []): string {
    $jwe = Build::jwe()
      ->alg('RSA-OAEP-256')
      ->enc('A128GCM')
      ->zip('DEF')
      ->payload(['d' => $data] + $claims);
    foreach ($headers as $headerKey => $headerValue) {
      $jwe->header($headerKey, $headerValue);
    }
    return $jwe->encrypt($this->keyStorage->getEncryptionKey());
  }

  protected function parseEncrypted(string $token) {
    /** @var \Jose\Easy\JWT $jwt */
    $jwt = Load::jwe($token)
      ->key($this->keyStorage->getEncryptionKey())
      ->run();
    return $jwt->claims->get('d');
  }

  protected function signAndEncrypt($data, array $headers = [], array $claims = []): string {
    $signed = $this->sign($data);
    return $this->encrypt($signed);
  }

  protected function parseSignedAndEncrypted(string $token) {
    $decrypted = $this->parseEncrypted($token);
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $signatureChecked = $this->parseSigned($decrypted);
    return $signatureChecked;
  }

  protected function packData($data) {
    $serialized = serialize($data);
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $base64 = base64_encode($serialized);
    return $base64;
  }

  protected function unpackData(string $base64) {
    $bytes = base64_decode($base64);
    if ($bytes === FALSE) {
      throw new \UnexpectedValueException('Not valid base64 data.');
    }
    // Unsrializing incoming data here is safe, as we only use signed data.
    $unserialized = @unserialize($bytes);
    if ($unserialized === FALSE && $bytes !== 'b:0;') {
      throw new \UnexpectedValueException('Not valid serialized data.');
    }
    return $unserialized;
  }

}
