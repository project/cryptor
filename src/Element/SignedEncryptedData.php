<?php

namespace Drupal\cryptor\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Annotation\FormElement;
use Drupal\Core\Render\Element\Hidden;
use Drupal\cryptor\Cryptor\CryptorService;

/**
 * Signed data.
 *
 * Specify either #default_value or #value but not both.
 *
 * @FormElement("cryptor_signed_encrypted")
 */
class SignedEncryptedData extends Hidden {

  use CryptorElementTrait;

  protected static function mustEncrypt(): bool {
    return TRUE;
  }

}
