<?php

namespace Drupal\cryptor\Element;

use Drupal\Core\Render\Annotation\FormElement;
use Drupal\Core\Render\Element\Hidden;

/**
 * Signed data.
 *
 * Specify either #default_value or #value but not both.
 *
 * @FormElement("cryptor_signed")
 */
class SignedData extends Hidden {

  use CryptorElementTrait;

  protected static function mustEncrypt(): bool {
    return FALSE;
  }

}
