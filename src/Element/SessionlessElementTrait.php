<?php

namespace Drupal\sessionless\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sessionless\CryptoService;

trait SessionlessElementTrait {

  protected static function getCryptoService(): CryptoService {
    return  \Drupal::service('sessionless.crypto_service');
  }
  public static function preRenderHidden($element) {
    $element['#value'] = self::getCryptoService()->dump($element['#value'], self::mustEncrypt());
    return parent::preRenderHidden($element);
  }

  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    return self::getCryptoService()->parse($element['#value'], self::mustEncrypt());
  }

  abstract protected static function mustEncrypt(): bool;

}
