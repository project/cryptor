<?php

namespace Drupal\cryptor\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\cryptor\CryptorService;

trait CryptorElementTrait {

  protected static function getCryptorService(): CryptorService {
    return  \Drupal::service('cryptor.service');
  }
  public static function preRenderHidden($element) {
    $element['#value'] = self::getCryptorService()->dump($element['#value'], self::mustEncrypt());
    return parent::preRenderHidden($element);
  }

  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    return self::getCryptorService()->parse($element['#value'], self::mustEncrypt());
  }

  abstract protected static function mustEncrypt(): bool;

}
