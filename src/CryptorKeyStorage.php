<?php

namespace Drupal\cryptor;

use Drupal\Core\State\StateInterface;
use Jose\Component\Core\JWK;
use Jose\Component\KeyManagement\JWKFactory;

/**
 * Key Storage
 *
 * @internal
 *
 * Notes:
 * - ECDSA signing does smaller signatures.
 *   https://auth0.com/blog/json-web-token-signing-algorithms-overview/
 * - AES-GCM is said to withstand a broader range of attacks
 *   https://isuruka.medium.com/selecting-the-best-aes-block-cipher-mode-aes-gcm-vs-aes-cbc-ee3ebae173c
 */
final class CryptorKeyStorage {

  protected StateInterface $state;

  public function __construct(StateInterface $state) {
    $this->state = $state;
  }


  public function getSignatureKey(): JWK {
    $stateKey = "cryptor.key.signature";
    $key = $this->state->get($stateKey);
    if (!$key) {
      $key = JWKFactory::createECKey('P-256');
      $this->state->set($stateKey, $key);
    }
    return $key;
  }

  public function getEncryptionKey(): JWK {
    $stateKey = "cryptor.key.encryption";
    $key = $this->state->get($stateKey);
    if (!$key) {
      $key = JWKFactory::createRSAKey(4096);
      $this->state->set($stateKey, $key);
    }
    return $key;
  }

}
