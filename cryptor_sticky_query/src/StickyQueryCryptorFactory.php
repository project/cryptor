<?php

namespace Drupal\cryptor_sticky_query;

use Drupal\cryptor\CryptorService;
use Drupal\sticky_query\StickyQuery\StickyQueryHandlerFactoryInterface;
use Drupal\sticky_query\StickyQuery\StickyQueryHandlerInterface;
use Drupal\sticky_query\StickyQuery\StickyQueryHandlerRegistry;

class StickyQueryCryptorFactory implements StickyQueryHandlerFactoryInterface {

  protected CryptorService $cryptor;

  protected StickyQueryHandlerInterface $signHandler;

  protected StickyQueryHandlerInterface $signEncHandler;

  public function __construct(CryptorService $cryptor) {
    $this->cryptor = $cryptor;
  }

  public function registerHandlers(StickyQueryHandlerRegistry $registry): void {
    $this->signHandler = new CryptorStickyQueryHandler('cryptor', FALSE, $this->cryptor);
    $registry->add($this->signHandler);
    $this->signEncHandler = new CryptorStickyQueryHandler('cryptor-enc', TRUE, $this->cryptor);
    $registry->add($this->signEncHandler);
    $t=$this;
  }

  public function getHandler(bool $encrypt): StickyQueryHandlerInterface {
    return $encrypt ? $this->signEncHandler : $this->signHandler;
  }

}
