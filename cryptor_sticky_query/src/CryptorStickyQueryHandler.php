<?php

namespace Drupal\cryptor_sticky_query;

use Drupal\cryptor\CryptorService;
use Drupal\sticky_query\StickyQuery\StickyQueryAppDrivenValueHandlerBase;
use Drupal\sticky_query\StickyQuery\StickyQueryHandlerBase;
use Drupal\sticky_query\StickyQuery\StickyQueryDefaultStorage;

class CryptorStickyQueryHandler extends StickyQueryHandlerBase {

  use StickyQueryAppDrivenValueHandlerBase;

  protected bool $mustEncrypt;

  protected CryptorService $cryptorService;

  public function __construct(string $key, bool $encrypt, CryptorService $cryptorService) {
    parent::__construct($key);
    $this->mustEncrypt = $encrypt;
    $this->cryptorService = $cryptorService;
  }


  public function storeInboundValue($value): void {
    if ($value && is_string($value)) {
      $this->setValue($this->cryptorService->parse($value, $this->mustEncrypt));
    }
    else {
      $this->setValue(NULL);
    }
  }

  public function mergeOutboundValue($hrefValue) {
    $data = $this->getValue();
    if ($data) {
      $jwtString = $this->cryptorService->dump($data, $this->mustEncrypt);
    }
    else {
      $jwtString = NULL;
    }
    return $jwtString;
  }

}
