<?php

namespace Drupal\cryptor_sticky_query;

use Drupal\sticky_query\StickyQuery\StickyQueryHandlerInterface;
use Symfony\Component\HttpFoundation\Session\SessionBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CryptorStickyQuerySession implements SessionInterface {

  protected bool $encrypt;

  protected StickyQueryCryptorFactory $factory;

  public function __construct(bool $encrypt, StickyQueryCryptorFactory $factory) {
    $this->encrypt = $encrypt;
    $this->factory = $factory;
  }

  private function getStickyQueryHandler(): StickyQueryHandlerInterface {
    return $this->factory->getHandler($this->encrypt);
  }

  public function start() {}

  public function getId() {
    return 'cryptor';
  }

  public function setId($id) {}

  public function getName() {
    return 'cryptor';
  }

  public function setName($name) {}

  public function invalidate($lifetime = NULL) {
    $this->getStickyQueryHandler()->setValue(NULL);
  }

  public function migrate($destroy = FALSE, $lifetime = NULL) {}

  public function save() {}

  public function has($name) {
    $array = $this->getStickyQueryHandler()->fetchOutboundValue();
    return isset($array[$name]);
  }

  public function get($name, $default = NULL) {
    $array = $this->getStickyQueryHandler()->fetchOutboundValue();
    return $array[$name] ?? $default;
  }

  public function set($name, $value) {
    $array = $this->getStickyQueryHandler()->fetchOutboundValue();
    $array[$name] = $value;
    $this->getStickyQueryHandler()->setValue($array);
  }

  public function all() {
    return $this->getStickyQueryHandler()->fetchOutboundValue();
  }

  public function replace(array $attributes) {
    $array = $this->getStickyQueryHandler()->fetchOutboundValue();
    $array = $attributes + $array;
    $this->getStickyQueryHandler()->setValue($array);
  }

  public function remove($name) {
    $array = $this->getStickyQueryHandler()->fetchOutboundValue();
    unset($array[$name]);
    $this->getStickyQueryHandler()->setValue($array);
  }

  public function clear() {
    $this->getStickyQueryHandler()->setValue(NULL);
  }

  public function isStarted() {
    return TRUE;
  }

  public function registerBag(SessionBagInterface $bag) {
    return new \LogicException();
  }

  public function getBag($name) {
    return new \LogicException();
  }

  public function getMetadataBag() {
    return new \LogicException();
  }

}
